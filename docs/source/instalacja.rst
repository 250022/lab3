Instalacja 
===========
Wymagania: 

Docker

WSL

Należy utworzyć potrzebne kontenery docker.
Następnie zrobić plik docker-compose.yaml, zawrzeć w nim informacje o wykorzystanych obrazach docker.
Potrzebny jest także plik .env zawierający informacje o środowisku.
Plik docker-compose zbudować w cmd komendą docker-compose build.