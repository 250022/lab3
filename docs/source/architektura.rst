Architektura
=============

Diagram klas
-------------------------

.. uml::

   class Customer{
	{static} int id
	{static} String firstName
        {static} String lastName
   }

   interface CustomerRepository{
	{static} List<Customer> findByLastName
	{static} Customer findById
   }

   class CustomerController{
        {static} CustomerRepository customerRepository
        +Iterable<Customer> getCustomers
        +Customer getUserById
   }

   Customer-* CustomerRepository
   CustomerRepository -> CustomerController


Diagram przypaków użycia
-------------------------

.. uml::

	left to right direction
	actor User as us

	package App{
	usecase "Show Customers List" as CL
	usecase "Show Customer Data" as CD
	}

	us-->CL
	us-->CD
