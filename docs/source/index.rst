.. lab3 documentation master file, created by
   sphinx-quickstart on Fri Jun  4 18:26:43 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to lab3's documentation!
================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`wprowadzenie`
* :ref:`architektura`
* :ref:`instalacja`
* :ref:`api`