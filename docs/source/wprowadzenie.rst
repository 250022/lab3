Wprowadzenie
================
Docker
------------------
Docker to program umożliwiający tworzenie obrazów i kontenerów. 
Wykorzystuje się go w celu zapamiętania warunków, 
w których został tworzony program (np. zapamiętanie wersji javy) i odtworzenia ich na innym urządzeniu.


Docker-compose
------------------
To plik pozwalający na otworzenie kilku kontenerów na raz.
Zawiera informacje o konfiguracji obrazów, którą chcemy otworzyć w danym czasie.